/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package u5_gui;

/**
 *
 * @author patata
 */
public class libreria {
    
    String cadena;
    boolean suma;
    boolean resta;
    boolean multiplicacion;
    boolean division;
    boolean raiz;
    double resultado;
    double resultadom=1d;

    public libreria() {
        cadena = "";
        suma = false;
        resta = false;
        multiplicacion = false;
        division = false;
        
    }
    
    public void suma(String cad){
        this.resultado +=Double.parseDouble(cad);
        suma=true;
        this.cadena="";
    }
    public void resta(String cad){
        this.resultado =Double.parseDouble(cad);
        
        resta=true;
        this.cadena="";
    }
    public void multiplicacion(String cad){
        
        this.resultadom *=Double.parseDouble(cad);
        multiplicacion=true;
        this.cadena="";
    }
    public void division(String cad){
        this.resultado =Double.parseDouble(cad);
        
        division=true;
        this.cadena="";
    }
    public void raiz(String cad){
        this.resultado=Double.parseDouble(cad);
        raiz=true;
        this.cadena="";
    }
    
    public String resultado1(String num){
        if(suma==true){
            resultado  = Double.parseDouble(num) + resultado;
        }
        if(resta==true){
            resultado  = resultado- Double.parseDouble(num) ;
        }
        if(multiplicacion==true){
            resultadom  = Double.parseDouble(num) * resultadom;
            resultado=resultadom;
        }
        if(division==true){
            resultado  = resultado/Double.parseDouble(num) ;
        }
        if(raiz==true){
            resultado  = Math.sqrt(Double.parseDouble(num)) ;
        }
        String resultado2=Double.toString(resultado);
        suma=false;
        resta=false;
        multiplicacion=false;
        division=false;
        raiz=false;
       
        return resultado2;
    }
    
    public void reset(){
        this.resultado=0;
        this.resultadom=1;
    }
    
}
