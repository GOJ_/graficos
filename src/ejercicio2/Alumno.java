/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import java.io.Serializable;

/**
 *
 * @author patata
 */
public class Alumno implements Serializable{
    private String dni;
    private String nombre;
    private String edad;
    private String curso;
    private float nota;
    private boolean repite;

    public Alumno(String dni, String nombre, String edad, String curso, float nota, boolean repite) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
        this.curso = curso;
        this.nota = nota;
        this.repite = repite;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

    public boolean isRepite() {
        return repite;
    }

    public void setRepite(boolean repite) {
        this.repite = repite;
    }
    
    
    
    
    
    
    
    
    
    
}
